# Final questionnaire
questions = {}
choices = {}

questions["clap"] = "Can you clap in time with a musical beat?"
choices["clap"]= ["Yes","No","I'm not sure"]

questions["musical_experience"]= "Have you ever played a musical instrument?"
choices["musical_experience"]=["Yes","No"]

questions["years_playing_music"] = """If yes, for how many years did you play? Please fill out even if the answer is zero."""

questions["which_instrument"] = """If yes, which instrument did you play?"""

questions["favourite_bands"] = """Please list at least three of your favorite bands or musical artists."""

questions["last_song_heard"] = """
        What was the last song or piece of music that you heard (earlier today / this morning / last night)? <br>
        Please type at least one name (this can be a genre if you don't remember the exact name)
        """

questions["dance"] = "Do you dance socially or professionally?"
choices["dance"] = [
    "Socially",
    "Professionally",
    "I never dance"
]

questions["last_dance"] = "If yes, when was the last time you danced? (choose the most accurate answer)"
choices["last_dance"] = [
    "This week",
    "This month",
    "This year",
    "Some years ago",
    "Many years ago",
    "I never dance"
]

questions["hearing_issues"] = "Do you have hearing loss or any other hearing issues?"
choices["hearing_issues"] = ["Yes","No"]

questions["education"] = "What is your highest level of formal education?"
choices["education"] = [
    "Pre-high school",
    "High school",
    "College",
    "Graduate School"
]

questions["violin"] = "How familiar were you with the sound of a violin before participating in the experiment?"
choices["violin"] = [
    "Very familiar",
    "Somewhat familiar",
    "Less familiar",
    "Not familiar at all"
]

questions["cello"] = "How familiar were you with the sound of a cello before participating in the experiment?"
choices["cello"] = [
    "Very familiar",
    "Somewhat familiar",
    "Less familiar",
    "Not familiar at all"
]

questions["guitar"] = "How familiar were you with the sound of a guitar before participating in the experiment?"
choices["guitar"] = [
    "Very familiar",
    "Somewhat familiar",
    "Less familiar",
    "Not familiar at all"
]

questions["trumpet"] = "How familiar were you with the sound of a trumpet before participating in the experiment?"
choices["trumpet"] = [
    "Very familiar",
    "Somewhat familiar",
    "Less familiar",
    "Not familiar at all"
]

questions["clarinet"] = "How familiar were you with the sound of a clarinet before participating in the experiment?"
choices["clarinet"] = [
    "Very familiar",
    "Somewhat familiar",
    "Less familiar",
    "Not familiar at all"
]

questions["saxophone"] = "How familiar were you with the sound of a saxophone before participating in the experiment?"
choices["saxophone"] = [
    "Very familiar",
    "Somewhat familiar",
    "Less familiar",
    "Not familiar at all"
]