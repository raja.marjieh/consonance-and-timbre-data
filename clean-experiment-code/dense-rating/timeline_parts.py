from flask import Markup
from psynet.timeline import (
    join
)
from psynet.page import (
    InfoPage,
)

from psynet.modular_page import (
    ModularPage,
    NumberControl,
    Prompt,
    PushButtonControl,
    TextControl
)

from . import questionnaire
from .questionnaire import (
    questions,
    choices
)

instructions_training = join(
    InfoPage(
        Markup("""
        <h1>Instructions</h1>
        <hr>
        <p>
            In this experiment, you will listen to sounds by moving a slider. <br>
            You will be asked to pick the sound which best represents a property in question.
        </p>
        <img src="/static/images/slider_example.png" alt="Schematic figure of experimental task" style="width:450px">
        <hr>
        """),
        time_estimate=10
    ),
    InfoPage(
        Markup("""
        <h1>Instructions (cont.):</h1>
        <h3>
            We will now play some training examples to help you understand the format of the experiment.
            To be able to submit a response you must explore at least three different locations of the slider.
        </h3>
        <img src="/static/images/explore.png" alt="Schematic figure of experimental task" style="width:450px">
        <hr>
        <p>
            <strong>Note:</strong> The quality of your responses will be checked automatically,
            and high quality responses will be bonused accordingly at the end of the experiment.
            The best strategy is to perform the task honestly and carefully, and then you will tend to give good results.
            Sometimes the sounds might take a moment to load, so please be patient.
        </p>
        <hr>
        """),
        time_estimate=10
    )
)

instructions_experiment = InfoPage(
    Markup("""
    <h1>Instructions (cont.):</h1>
    <h3>
        The actual experiment will begin now. Pay careful attention to the various sounds!
        Sometimes the differences between the sounds can be subtle, choose what seems most accurate to you.
    </h3>
    <p>
        <strong>Remember:</strong> the best strategy is just to honestly report what you think sounds better!
        You must explore at least three locations of the slider before submitting a response.
        Also, sometimes the sounds might take a moment to load, so please be patient.
    </p>
    """),
    time_estimate=3
)

final_questionnaire= join(
    InfoPage(
        Markup("""
        <h3>Thank you for completing the task!</h3>
        <br>
        Before we finish,
        please answer a few final questions.
        """),
        time_estimate=3
    ),
    ModularPage(
        "age",
        Prompt("Please indicate your age in years."),
        control=NumberControl(),
        time_estimate=3
    ),
    ModularPage(
        "gender",
        Prompt("What is your gender?"),
        control= PushButtonControl(["Female", "Male", "Other", "Prefer not to specify"],arrange_vertically=False),
        time_estimate=3
    ),
    ModularPage(
        "country",
        Prompt("""
            What country are you from?
            """),
        control=TextControl(one_line=True),
        time_estimate=3
    ),
    ModularPage(
        "mother_tongue",
        Prompt("""
            What is your mother tongue?
            """),
        control=TextControl(one_line=True),
        time_estimate=3
    ),
    ModularPage(
        "musical_experience",
        Prompt(questions["musical_experience"]),
        control=PushButtonControl(choices["musical_experience"],arrange_vertically=False),
        time_estimate=3
    ),
    ModularPage(
        "years_playing_music",
        Prompt(questions["years_playing_music"]),
        control=NumberControl(),
        time_estimate=3
    ),
    ModularPage(
        "which_instrument",
        Prompt(questions["which_instrument"]),
        control=TextControl(one_line=True),
        time_estimate=3
    ),
    ModularPage(
        "favourite_bands",
        Prompt(questions["favourite_bands"]),
        control=TextControl(one_line=True),
        time_estimate=3
    ),
    ModularPage(
        "last_song_heard",
        Prompt(Markup(questions["last_song_heard"])),
        control=TextControl(one_line=True),
        time_estimate=3
    ),
    ModularPage(
        "hearing_issues",
        Prompt(questions["hearing_issues"]),
        control=PushButtonControl(choices["hearing_issues"],arrange_vertically=False),
        time_estimate=3
    ),
    ModularPage(
        "education",
        Prompt(questions["education"]),
        control=PushButtonControl(choices["education"],arrange_vertically=False),
        time_estimate=3
    ),
    ModularPage(
        "feedback",
        Prompt("""
            Did you like the experiment?
            """),
        control=TextControl(one_line=False),
        time_estimate=5
    ),
    ModularPage(
        "technical_problems",
        Prompt(Markup("""
            Did you encounter any technical problems during the
            experiment? <br>If so, please provide a few words describing the
            problem.
            """)),
        control=TextControl(one_line=False),
        time_estimate=5
    )
)

