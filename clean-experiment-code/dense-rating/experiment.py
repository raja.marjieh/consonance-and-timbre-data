# pylint: disable=unused-import,abstract-method,unused-argument,no-member

##########################################################################################
#### Imports
##########################################################################################

import logging
import rpdb
import random
import re
import time
import os
import warnings

from typing import Union, List
from dallinger import db
from flask import Markup
from statistics import mean
from sqlalchemy import exc as sa_exc

from psynet.consent import MTurkStandardConsent

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)

import psynet.experiment
from psynet.timeline import (
    Timeline,
    join,
    PageMaker
)
from psynet.page import (
    InfoPage,
    SuccessfulEndPage,
    NumberInputPage,
    NAFCPage,
    TextInputPage,
    VolumeCalibration
)
from psynet.trial.static import (
    StimulusSpec,
    StimulusVersionSpec,
    StimulusSet,
    StaticTrialMaker,
    StaticTrial
)
from psynet.modular_page import ModularPage, Prompt, NAFCControl
from psynet.prescreen import HeadphoneTest

from . import timeline_parts
from .timeline_parts import (
    final_questionnaire
)

CONFIG = {
    "num_experiment_trials": 80, #PER BLOCK! 
    "num_repeat_trials": 5,
    "num_of_blocks": 1
}

INITIAL_RECRUITMENT_SIZE = 1

# REMEMBER TO CHECK ROLL OFF VALUE in synthesis.js!

class ChordPrompt(Prompt):
    macro = "chord_prompt"
    external_template = "chord_prompt.html"

    def __init__(self, chord, **kwargs):
        super().__init__(**kwargs)
        self.chord = chord

    @property
    def metadata(self):
        return {
            **super().metadata,
            "chord": self.chord
        }


def make_timeline():
    return Timeline(
        MTurkStandardConsent(),
        VolumeCalibration(),
        HeadphoneTest(),
        InfoPage("You passed the headphone screening task! Congratulations.", time_estimate=3),
        instructions(),
        InfoPage(
            f"""
            You will take up to
            {CONFIG['num_experiment_trials']*CONFIG["num_of_blocks"] + CONFIG['num_repeat_trials']} trials
            where you have to answer this question. Remember to pay careful attention
            in order to get the best bonus!
            """,
            time_estimate=5
        ),
        make_experiment_trials(),
        final_questionnaire,
        SuccessfulEndPage()
    )

def instructions():
    return join(
        InfoPage(Markup(
            """
            <p>
                In each trial of this experiment you will be presented with a word
                and a sound. Your task will be to judge how well the sound
                matches the word.
            </p>
            <p>
                You will have seven response options, ranging from 'Completely Disagree' to 'Completely Agree'.
                Choose the one you think is most appropriate.
            </p>
            """),
            time_estimate=5
        ),
        InfoPage(
            """
            The quality of your responses will be automatically monitored,
            and you will receive a bonus at the end of the experiment
            in proportion to your quality score. The best way to achieve
            a high score is to concentrate and give each trial your best attempt.
            """,
            time_estimate=5
        )
    )

def get_stimulus_set(phase: str): 
    import pandas as pd
    import json
    phase = "experiment"
    df = pd.read_csv("chord-states-dyads-test.csv") # Change to desired chord file
    stimuli = [
        StimulusSpec(
            {
                "chord": {
                    "f0": record["f0"],
                    "intervals": json.loads(record["intervals"]),
                    "synth": record["synth"],
                    "parametrization": record["parametrization"],
                    "shepardize": record["shepardize"],
                    "rolloff": record.get("rolloff"),
                    "custom_timbre": record.get("custom_timbre"),
                    "group": record.get("group"),
                    "block": record.get("block") if record.get("block") else "default",
                    "time_to_wait_in_sec": 1.4 # disables rating buttons until chord ends
                    },
                "target": record["target"],
                "type": record["type"],
            },
            phase=phase,
            block= record.get("block") if record.get("block") else "default"
        )
        for record in df.to_dict("records")
    ]
    return StimulusSet("chord_stimuli", stimuli)

nafc_choices = [
    (1, "(1) Completely Disagree"),
    (2, "(2) Strongly Disagree"),
    (3, "(3) Disagree"),
    (4, "(4) Neither Agree nor Disagree"),
    (5, "(5) Agree"),
    (6, "(6) Strongly Agree"),
    (7, "(7) Completely Agree")
]

class RatingControl(NAFCControl):
    def __init__(self):
        super().__init__(
            choices=[x[0] for x in nafc_choices],
            labels=[x[1] for x in nafc_choices],
            arrange_vertically=False
            # min_width="125px"
        )

    def format_answer(self, raw_answer, **kwargs):
        return int(raw_answer)

class CustomTrial(StaticTrial):
    __mapper_args__ = {"polymorphic_identity": "custom_trial"}

    def show_trial(self, experiment, participant):
        prompt = Markup(
            f"""
            <p>How well does the sound match the following word (pay attention to subtle differences):</p>
            <p><strong>{self.definition['target']}</strong></p>
            """
        )

        return ModularPage(
            "custom_trial",
            ChordPrompt(self.definition["chord"], text=prompt, text_align="center"),
            RatingControl()
        )

class CustomTrialMaker(StaticTrialMaker):
    give_end_feedback_passed = False
    performance_threshold = -1.0

    def compute_bonus(self, score, passed):
        if self.phase == "practice":
            return 0.0
        elif self.phase == "experiment":
            if score is None:
                return 0.0
            else:
                return min(max(0.0, 0.5*score),0.5)
        else:
            raise NotImplementedError

def make_experiment_trials():
    return CustomTrialMaker(
        id_="main_experiment",
        trial_class=CustomTrial,
        phase="experiment",
        stimulus_set=get_stimulus_set(phase="experiment"),
        time_estimate_per_trial=4,
        recruit_mode="num_trials",
        target_num_participants=None,
        target_num_trials_per_stimulus=1,
        max_trials_per_block=CONFIG["num_experiment_trials"],
        allow_repeated_stimuli=False,
        max_unique_stimuli_per_block=None,
        active_balancing_within_participants=True,
        active_balancing_across_participants=True,
        check_performance_at_end=True,
        check_performance_every_trial=False,
        fail_trials_on_premature_exit=True,
        fail_trials_on_participant_performance_check=False,
        num_repeat_trials=CONFIG["num_repeat_trials"]
    )

class Exp(psynet.experiment.Experiment):
    timeline = make_timeline()

    def __init__(self, session=None):
        super().__init__(session)
        self.initial_recruitment_size = INITIAL_RECRUITMENT_SIZE

