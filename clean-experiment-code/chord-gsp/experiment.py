# pylint: disable=unused-import,abstract-method,unused-argument,no-member

##########################################################################################
#### Imports
##########################################################################################

from flask import Markup
from statistics import mean
import random
import re
from typing import Union, List
import time
import json
import csv
from statistics import median
import rpdb
import itertools
import numpy as np
import statsmodels.api as sm
from typing import Optional, Dict

import psynet.experiment

from psynet.field import claim_field
from psynet.participant import Participant, get_participant
from psynet.timeline import (
    Page,
    Timeline,
    PageMaker,
    CodeBlock,
    while_loop,
    conditional,
    switch,
    FailedValidation
)
from psynet.page import (
    InfoPage,
    SuccessfulEndPage,
    NAFCPage,
    NumberInputPage,
    VolumeCalibration,
    Prompt
)
from psynet.modular_page import ModularPage, SliderControl
from psynet.prescreen import HeadphoneTest
from psynet.trial.chain import ChainNetwork
from psynet.trial.gibbs import (
    GibbsNetwork, GibbsTrial, GibbsNode, GibbsSource, GibbsTrialMaker
)

from psynet.consent import MTurkStandardConsent

from . import timeline_parts
from .timeline_parts import (
    instructions_training,
    instructions_experiment,
    final_questionnaire
)

from .experiment_utils import (
    generate_mode
)

import logging
import os

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)

# import rpdb

##########################################################################################
#### Global parameters
##########################################################################################

TARGETS = [{"target": "pleasant"}]
SYNTH_TYPE = ["harmonic"] # can be "pure", "harmonic", "stretched", "compressed", "piano", "xylophone", "violin", "guitar", "saxophone", "harpsichord", "clarinet", "trumpet", "flute"
PARAMETRIZATION = ["cumulative"] # can be "bass", "cumulative" or "centralized"
PROGRESSIONS = 0 # number of progressions, 0 means 1 chord, 1 means 2 chords etc 
SHEPARDIZE = [False] # Use Shepard tones
LATERALIZE = False # lateralizes tones 
STRETCH = False
FREE_TIMBRE = False # Whether to manipulate timbre partial amplitudes or not
TIMBRE_PARAMETRIZATION = "euclidean" # can be "euclidean" or "spherical"
TDIM = 4 # Number of timbre dimensions to manipulate, must be < NH in synthesis.js (corresponds to tdim+1 harmonics)
FREE_VOLUME = False # Whether to manipulate volume or not
NUM_INTERVALS = 2 # number of tones in a chord - 1
INTERVAL_RANGE = [0.5, 8.5] # Standard is [0.5,7.5] 
FREE_MEAN_PITCH = False
F0 = 60.0 # Frequency center around which the chord is built. Use 60.0 for bass and cumulative parametrizations and 67.0 for centralized.
RELATIVE_BASE_RANGE = [-5, 5]
LATERALIZATION = [-1, 1]
STRETCHING = [1.8,2.15]
MEAN_PITCH_RANGE = [55+4,65+4] # mean frequency range for when the mean pitch equilibrization is activated (neutralizes F0)
VOLUME_RANGE = [-np.log(TDIM+1)/2 - np.log(2),np.log(TDIM+1)/2 - np.log(2)] 

# REMEMBER TO CHECK ROLL OFF VALUE in synthesis.js!

#GENERAL
INITIAL_RECRUITMENT_SIZE = 1
TIME_ESTIMATE_PER_TRIAL = 16
NUM_OF_AGGREGATIONS = 1

MODE_SPECS = {
    "num_intervals": NUM_INTERVALS,
    "num_progressions": PROGRESSIONS,
    "int_range": INTERVAL_RANGE,
    "bass_rel_range": RELATIVE_BASE_RANGE,
    "lateral": LATERALIZE,
    "lateral_range": LATERALIZATION,
    "stretch": STRETCH, 
    "inharmonicity_range": STRETCHING,
    "free_timbre": FREE_TIMBRE,
    "timbre_param": TIMBRE_PARAMETRIZATION,
    "tdim": TDIM,
    "free_volume": FREE_VOLUME,
    "volume_range": VOLUME_RANGE,
    "free_mean_pitch": FREE_MEAN_PITCH,
    "mean_pitch_range": MEAN_PITCH_RANGE
}


# TRAINING MODE
MODE_TRAIN = []
for target in TARGETS:
    MODE_TRAIN = MODE_TRAIN + [generate_mode(MODE_SPECS, target)]

# EXPERIMENT MODE
MODES = []
for target in TARGETS:
    MODES = MODES + [generate_mode(MODE_SPECS, target)]

DIMENSIONS = [key for key in MODES[0]] ## DICTATES GIBBS LOOPING ORDER
DIMENSIONS.remove("target")


class ChordSliderControl(SliderControl):
    def __init__(
        self,
        label: str,
        start_value: float,
        min_value: float,
        max_value: float,
        f0: float,
        sliders: List[str],
        synth: str,
        parametrization: str,
        shepardize: bool,
        num_steps: int = 10000,
        reverse_scale: Optional[bool] = False,
        directional: Optional[bool] = True,
        slider_id: Optional[str] = "sliderpage_slider",
        input_type: Optional[str] = "HTML5_range_slider",
        snap_values: Optional[Union[int, list]] = None,
        minimal_interactions: Optional[int] = 3,
        minimal_time: Optional[float] = 3.0,
        continuous_updates: Optional[bool] = False,
        template_filename: Optional[str] = None,
        template_args: Optional[Dict] = None,
        fixed_chord = None
    ):
        super().__init__(
            label=label,
            start_value=start_value,
            min_value=min_value,
            max_value=max_value,
            num_steps=num_steps,
            reverse_scale=reverse_scale,
            directional=directional,
            slider_id=slider_id,
            input_type=input_type,
            snap_values=snap_values,
            minimal_interactions=minimal_interactions,
            minimal_time=minimal_time,
            continuous_updates=continuous_updates,
            template_filename=template_filename,
            template_args=template_args     
        )

        js_vars = self.js_vars
        js_vars["f0"] = f0
        js_vars["sliders"] = sliders
        js_vars["synth"] = synth
        js_vars["parametrization"] = parametrization
        js_vars["shepardize"] = shepardize
        js_vars["fixed_chord"] = fixed_chord
        self.js_vars = js_vars



class ChordSliderPage(ModularPage):
    def __init__(
        self,
        label: str,
        prompt: Union[str, Markup],
        selected_idx: int,
        starting_values: List[float],
        directional: bool,
        f0: float,
        min_value: float, 
        max_value: float,
        sliders: List[str],
        synth: str,
        parametrization: str,
        shepardize: bool,
        reverse_scale: bool,
        minimal_interactions: float,
        minimal_time: float,
        fixed_chord=None,
        time_estimate=None,
        **kwargs,
    ):
        assert selected_idx >= 0 and selected_idx < len(DIMENSIONS)
        self.prompt = prompt
        self.selected_idx = selected_idx
        self.starting_values = starting_values

        not_selected_idxs = list(range(len(DIMENSIONS)))
        not_selected_idxs.remove(selected_idx)
        not_selected_intervals = [DIMENSIONS[i] for i in not_selected_idxs]
        not_selected_values = [starting_values[i] for i in not_selected_idxs]
        hidden_inputs = dict(zip(not_selected_intervals, not_selected_values))

        kwargs["template_arg"] = {
            "hidden_inputs": hidden_inputs,
        }
        
        super().__init__(
            label,
            Prompt(prompt),
            control=ChordSliderControl(
                label=label,
                start_value=starting_values[selected_idx],
                slider_id=DIMENSIONS[selected_idx],
                reverse_scale=reverse_scale,
                directional=directional,
                template_filename="chord-slider.html",
                template_args={
                    "hidden_inputs": hidden_inputs,
                },
                continuous_updates=False,
                f0=f0,
                sliders=sliders,
                synth=synth,
                parametrization=parametrization,
                shepardize=shepardize,
                fixed_chord=fixed_chord,
                min_value=min_value,
                max_value=max_value,
                minimal_interactions=minimal_interactions,
                minimal_time=minimal_time

            ),
            time_estimate=time_estimate,
        )

    def metadata(self, **kwargs):
        return {
            "prompt": self.prompt.metadata,
            "selected_idx": self.selected_idx,
            "starting_values": self.starting_values,
        }



class CustomNetwork(GibbsNetwork):
    __mapper_args__ = {"polymorphic_identity": "custom_network"}

    vector_length = len(DIMENSIONS)

    def random_sample(self, i):
        par_range = self.definition["mode"][DIMENSIONS[i]] ## DIMENSIONS DICTATES LOOPING ORDER!
        assert par_range[0] < par_range[1]
        return par_range[0] + (par_range[1] - par_range[0])*random.random()

    def make_definition(self):
        if self.phase=="training":
            definition = {
               "mode": random.choice(MODE_TRAIN), 
               "synth": random.choice(SYNTH_TYPE),
               "parametrization": random.choice(PARAMETRIZATION),
               "shepardize": random.choice(SHEPARDIZE)
            }
        else:
            definition = {
                "mode": self.balance_across_networks(MODES),
                "synth": self.balance_across_networks(SYNTH_TYPE),
                "parametrization": self.balance_across_networks(PARAMETRIZATION),
                "shepardize": self.balance_across_networks(SHEPARDIZE)
            }
        return definition

    # Minimal example of an async_post_grow_network function
    run_async_post_grow_network = False
    def async_post_grow_network(self):
        logger.info("Running custom async_post_grow_network function (network id = %i)", self.id)


class CustomTrial(GibbsTrial):
    __mapper_args__ = {"polymorphic_identity": "custom_trial"}

    # If True, then the starting value for the free parameter is resampled
    # on each trial.
    resample_free_parameter = True
    minimal_interactions = 3
    minimal_time = 3.0

    def show_trial(self, experiment, participant):
        # selected_interval = INTERVALS[self.active_index]
        mode = self.network.definition["mode"]
        target = mode["target"]["target"] 
        par_range = mode[DIMENSIONS[self.active_index]]  ## *DIMENSIONS* ALWAYS DICTATES LOOPING ORDER IN GIBBS!
        if "fixed_chord" in mode["target"]:
            fixed_chord = mode["target"]["fixed_chord"]
        else:
            fixed_chord = None
        prompt = Markup(
            "Adjust the slider to match the following word as well as possible: "
            f"<big><strong>{target}</strong>.</big>"
            "<p> Please pay attention to the subtle differences. </p>"
        )
        return ChordSliderPage(
            "chord_trial",
            prompt,
            starting_values=self.initial_vector,
            selected_idx=self.active_index,
            reverse_scale=self.reverse_scale,
            time_estimate=TIME_ESTIMATE_PER_TRIAL,
            minimal_interactions = self.minimal_interactions,
            minimal_time = self.minimal_time,
            min_value=par_range[0],
            max_value=par_range[1],
            f0=self.definition["f0"],
            sliders = self.definition["sliders"],
            synth = self.definition["synth"],
            parametrization = self.definition["parametrization"],
            shepardize = self.definition["shepardize"],
            fixed_chord = fixed_chord,
            directional=False
        )

    def make_definition(self, experiment, participant):
        definition = super().make_definition(experiment, participant)
        definition["f0"] = F0 + 10 * (random.random() - 0.5)
        definition["sliders"] = DIMENSIONS                                  ## DICTATES GIBBS LOOPING
        definition["synth"] = self.network.definition["synth"]
        definition["parametrization"] = self.network.definition["parametrization"]
        definition["shepardize"] = self.network.definition["shepardize"]
        return definition

    # Minimal example of an async_post_trial function
    run_async_post_trial = False
    def async_post_trial(self):
        logger.info("Running custom async post trial (id = %i)", self.id)

class CustomNode(GibbsNode):
    __mapper_args__ = {"polymorphic_identity": "custom_node"}

    summarize_trials_method = "kernel_mode"
    kernel_width = 0.2

    def kernel_summarize(self, observations, method):
        assert isinstance(observations, list)

        if (len(observations) == 1):
            return observations[0]

        kernel_width = self.scaled_width()
        if (not isinstance(kernel_width, str)) and (np.ndim(kernel_width) == 0):
            kernel_width = [kernel_width]

        density = sm.nonparametric.KDEMultivariate(
            data=observations,
            var_type="c",
            bw=kernel_width
        )

        points_to_evaluate = np.linspace(min(observations), max(observations), num=501)
        pdf = density.pdf(points_to_evaluate)

        if method == "mode":
            index_max = np.argmax(pdf)
            mode = points_to_evaluate[index_max]

            self.var.summary_kernel = {
                "bandwidth": kernel_width,
                "index_max": int(index_max),
                "mode": float(mode),
                "observations": observations,
                "pdf_locations": points_to_evaluate.tolist(),
                "pdf_values": pdf.tolist()
            }
            return mode
        else:
            raise NotImplementedError

    def scaled_width(self):
        par_range = self.network.definition["mode"][DIMENSIONS[self.active_index]]
        # return (par_range[1] - par_range[0]) * self.kernel_width # applies appropriate kernel sigma per dimension.
        return 0.3



class CustomSource(GibbsSource):
    __mapper_args__ = {"polymorphic_identity": "custom_source"}

    def generate_seed(self, network, experiment, participant):
        if network.vector_length is None:
            raise ValueError("network.vector_length must not be None. Did you forget to set it?")
        seed_spec = {
            "vector": [network.random_sample(i) for i in range(network.vector_length)],
            "active_index": random.randint(0, network.vector_length - 1)
        }
        return seed_spec

class CustomTrialMaker(GibbsTrialMaker):
    give_end_feedback_passed = False
    performance_threshold = -1.0

    def get_end_feedback_passed_page(self, score):
        score_to_display = "NA" if score is None else f"{(100 * score):.0f}"

        return InfoPage(
            Markup(f"Your consistency score was <strong>{score_to_display}&#37;</strong>."),
            time_estimate=5
        )

    def compute_bonus(self, score, passed):
        if score is None:
            return 0.0
        else:
            return min(max(0.0, 1 * (score - 0.5)),0.5)/2 # chance-rate is 0.5


trial_maker_training = CustomTrialMaker(
    id_="GSP_chord_training",
    network_class=CustomNetwork,
    trial_class=CustomTrial,
    node_class=CustomNode,
    source_class=CustomSource,
    phase="training",  # can be whatever you like
    time_estimate_per_trial=TIME_ESTIMATE_PER_TRIAL,
    chain_type="within",  # can be "within" or "across"
    num_trials_per_participant=6,
    num_iterations_per_chain=4, # note that the final node receives no trials
    num_chains_per_participant=1,  # set to None if chain_type="across"
    num_chains_per_experiment=None,  # set to None if chain_type="within"
    trials_per_node=1,
    balance_across_chains=False,
    check_performance_at_end=False,
    check_performance_every_trial=False,
    propagate_failure=False,
    recruit_mode="num_participants",
    target_num_participants=0
)

### FOR ACTUAL EXPERIMENTS
trial_maker_experiment = CustomTrialMaker(
    id_="GSP_chord_experiment",
    network_class=CustomNetwork,
    trial_class=CustomTrial,
    node_class=CustomNode,
    source_class=CustomSource,
    phase="experiment",  # can be whatever you like
    time_estimate_per_trial=TIME_ESTIMATE_PER_TRIAL,
    chain_type="across",  # can be "within" or "across"
    num_trials_per_participant=40, #FOR REGULAR TRIAD TASKS SET THIS AND NUM_NODES_PER_CHAIN TO 40
    num_iterations_per_chain=40, # note that the final node receives no trials
    num_chains_per_participant=None,  # set to None if chain_type="across"
    num_chains_per_experiment=50,  # set to None if chain_type="within" 
    trials_per_node=NUM_OF_AGGREGATIONS,
    balance_across_chains=False,
    check_performance_at_end=True,
    check_performance_every_trial=False,
    propagate_failure=False,
    recruit_mode="num_trials",
    target_num_participants=None,
    num_repeat_trials=3
)

### FOR TESTING WITHIN CHAINS
# trial_maker_experiment = CustomTrialMaker(
#     id_="GSP_chord_experiment",
#     network_class=CustomNetwork,
#     trial_class=CustomTrial,
#     node_class=CustomNode,
#     source_class=CustomSource,
#     phase="experiment",  # can be whatever you like
#     time_estimate_per_trial=TIME_ESTIMATE_PER_TRIAL,
#     chain_type="within",  # can be "within" or "across"
#     num_trials_per_participant=100,
#     num_iterations_per_chain=45 + 1, # note that the final node receives no trials
#     num_chains_per_participant=1,  # set to None if chain_type="across"
#     num_chains_per_experiment=None,  # set to None if chain_type="within"
#     trials_per_node=NUM_OF_AGGREGATIONS,
#     balance_across_chains=True,
#     check_performance_at_end=True,
#     check_performance_every_trial=False,
#     propagate_failure=False,
#     recruit_mode="num_participants",
#     target_num_participants=10,
#     num_repeat_trials=3
# )


##########################################################################################
#### Experiment
##########################################################################################

class Exp(psynet.experiment.Experiment):
    timeline = Timeline(
        MTurkStandardConsent(),
        # VolumeCalibration(),
        # HeadphoneTest(),
        InfoPage("You passed the headphone screening task! Congratulations.", time_estimate=3),
        instructions_training,
        trial_maker_training,
        instructions_experiment,
        trial_maker_experiment,
        final_questionnaire,
        SuccessfulEndPage()
    )

    def __init__(self, session=None):
        super().__init__(session)

        # Change this if you want to simulate multiple simultaneous participants.
        self.initial_recruitment_size = INITIAL_RECRUITMENT_SIZE


