
var default_dict = {
  "attack": 0.2,
  "decay": 0.1,
  "sustain_amp": 0.8,
  "duration": 1,
  "reg": 0.1,
  "NH": 10, // default_dict["NH"] should be the maximum number of harmonics that is needed for any synthesis given the shepardize value
  "inharmonicity": 2, // 2 is the harmonic series
  "synth": "harmonic",
  "parametrization": "bass",
  "shepardize": false,
  "NSH": 10, // the number of octave transpositions used to create a Shepard tower (runs from -NSH to NSH). 
            // With NH=10 harmonics, can go up to NSH=5 without crashing, though it's too small for inversion invariance. 
            // Can do pure with NSH=10 by setting NH=1, then harmonic = pure or NSH=10 and NH = 4 (recovers Nori's chords)
  "rolloff": 3 
}


play_chord = function (active_nodes, chord_dict) {
    // Generate a chord, the number of notes involved is specified by the list of intervals
    // The input is provided by a dictionary of the following form
    // chord_dict{
    //   "f0": float // bass tone frequency in midi,
    //   "intervals": [int1,int2,int3,...] // list of intervals from bass tone in midi
    //   "synth": str // the type of synthesis, e.g. "pure", "harmonic", "stretched", "compressed", "piano", "xylophone", "violin", "guitar"
    //   "inharmonicity": the amount of inharmonic stretching desired
    //   "parametrization": how the chord intervals are parametrized
    // }
  
    var chord = {...chord_dict}
    var intervals = [0].concat(chord["intervals"])
    var N = intervals.length
  
    for (key in default_dict) {
      if (!(key in chord)){
        chord[key] = default_dict[key]
      }
    }

    chord["NH"]=get_NH_NSH(chord["shepardize"])[0]
    chord["NSH"]=get_NH_NSH(chord["shepardize"])[1]

    var weights = util_complex(chord["NH"],chord["rolloff"]);

    if (chord["synth"] == "pure") {
      timbre = new Array(chord["NH"]).fill(0);
      timbre[0] = weights[0];
      chord["inharmonicity"] = 2;
    } else if (chord["synth"] == "harmonic"){
      timbre = weights
      chord["inharmonicity"] = 2;
    } else if (chord["synth"] == "stretched"){
      timbre = weights
      chord["inharmonicity"] = 2.1;   
    } else if (chord["synth"] == "compressed"){
      timbre = weights
      chord["inharmonicity"] = 1.9; 
    } else if (chord["synth"] == "gamelan"){
      timbre = new Array(chord["NH"]).fill(0);
      for (n=0;n<4;n++){
        timbre[n] = 1
      }
      chord["inharmonicity"] = 2;
    }

    if (chord["stretch"].length>0){
      chord["inharmonicity"] = chord["stretch"][0]
    }

  
    if (chord["custom_timbre"].length>0){

      if (chord["timbre_param"] == "euclidean"){
        timbre = [1].concat(chord["custom_timbre"]) 
        //// normalize timbre
        // nn = norm(timbre)
        // timbre = timbre.map(x => x/nn)
      } else if (chord["timbre_param"] == "spherical"){
        timbre = angle_to_point(chord["custom_timbre"]) 
      }
      
      console.assert(chord["NH"] - timbre.length >= 0, "Length of custom timbre must not exceed %d!", chord["NH"])
      topad = new Array(chord["NH"] - timbre.length).fill(0);
      timbre = timbre.concat(topad)
    }

    if (chord["volume"].length>0){
      vol = Math.min(Math.exp(chord["volume"]),5) //clipping added for saftey
      timbre = timbre.map(x => vol * x)
    }

    freqs = []
    current = chord["f0"]
    for (i=0;i<N;i++){
      if (chord["parametrization"] == "bass"){
        freqs = freqs.concat([util_midi2freq(chord["f0"] + intervals[i])])
      } else if (chord["parametrization"] == "cumulative") {
        current = current + intervals[i]
        freqs = freqs.concat([util_midi2freq(current)])
      }
    }
    
    // Implements a centralized representation which means cumulative forward and backward representations around the middle tone
    if (chord["parametrization"] == "centralized"){
      freqs = []
      intervals = chord["intervals"]

      chord_center = Math.floor(intervals.length / 2)
      upper_intervals = intervals.slice(chord_center,intervals.length + 1)
      upper_intervals = [0].concat(upper_intervals)
      lower_intervals = intervals.slice(0,chord_center)

      current = chord["f0"]
      for (ii=0;ii<upper_intervals.length;ii++){
        current = current + upper_intervals[ii]
        freqs = freqs.concat([util_midi2freq(current)]) 
      }

      current = chord["f0"]
      for (iii=lower_intervals.length-1;iii>=0;iii--){
        current = current - lower_intervals[iii]
        freqs = freqs.concat([util_midi2freq(current)]) 
      }
    }

    // Implements chord equilibrization
    if (chord["mean_pitch"].length>0){
      freqs = equilibrize_chord(freqs,util_midi2freq(chord["mean_pitch"][0]))
    }
    
    if (INST_NAMES.includes(chord["synth"])){
      instrument = INSTRUMENTS[chord["synth"]]
      instrument.triggerAttackRelease(freqs, 0.9)
    } else {
      custom_timbre_synth(active_nodes,freqs,timbre,chord)
    }  
  
}
  
util_freq2midi = function (freq) {
    return Math.log2(freq/440)*12 + 69
}

util_midi2freq = function (midi) {
    return (Math.pow(2,(midi-69)/12))*440
}

util_complex = function (NH,rolloff) {
    var partials = []
    var norm = 0

    for (i=1;i<=NH;i++){
        weight = - Math.log2(i) * rolloff //make it programmatic through chord
        weight = Math.pow(10,weight/20) // decays at the rate of 12 dB/octave
        partials = partials.concat([weight])
        norm = norm + Math.pow(weight,2)
    }

    return partials.map(x => x/Math.sqrt(norm))

}

util_shepard = function (NSH,NSH_max,freq,inharmonicity) {
  var weights = []
  var norm = 0
  gamma = Math.log2(inharmonicity) // inharmonic rescaling factor

  for (n=0;n<2*NSH+1;n++){
      curr_freq = util_freq2midi(freq * Math.pow(inharmonicity,n - NSH))
      weight = util_gaussian(curr_freq,gamma*65.5,gamma*8.2) // a Gaussian weight centered at the mid point of the midi scale, rescaled if needed for inharmonic compatability
      weights = weights.concat([weight])
      norm = norm + Math.pow(weight,2)
  }

  weights = weights.map(x => x/Math.sqrt(norm))

  padding = new Array(NSH_max-NSH).fill(0); // symmetric padding around the central weights to keep a fixed size
  weights = padding.concat(weights)
  weights = weights.concat(padding)

  return weights

}

util_gaussian = function(x,mu,sigma){
  N = Math.sqrt(2*Math.PI*(sigma**2))
  return 1/N * Math.exp(-1 * ((x - mu) ** 2) / (2 * sigma ** 2))
}

custom_timbre_synth = function(active_nodes,freqs,timbre,chord){
  var NH = timbre.length;
  var ampEnv = active_nodes["envelope"];

  if (chord["shepardize"]){
    NSH = chord["NSH"] 
  } else {
    NSH = 0
  }

  NH_max=get_NH_NSH(chord["shepardize"])[0] // the NH used in the setter function
  NSH_max=get_NH_NSH(chord["shepardize"])[1] // the NSH used in the setter function

  for (i=0;i<freqs.length;i++){
    freq = freqs[i]
    tone_nodes = active_nodes["complex_" + String(i)]
    // generate shepard weight tower around freq of width NSH, and then zero-pad to width default_dict["NSH"]
    sweights = util_shepard(NSH,NSH_max,freq,chord["inharmonicity"]) 
    // default_dict["NH"] captures the actual number of nodes used in the synethsizer setter 
    for (j=0;j<2*NSH_max+1;j++){ 
      // generate Shepard octave compatible with stretching 
      curr_freq = freq * Math.pow(chord["inharmonicity"],j - NSH_max) 
      for (k=0;k<NH;k++){ 
        osc = tone_nodes[j][k][0]
        gain = tone_nodes[j][k][1]

        if (chord["synth"] == "gamelan" && i>0) {

          freq_vals = get_custom_freqs(chord["synth"],NH)
          osc.frequency.value = curr_freq * freq_vals[k]

        } else {

          osc.frequency.value = curr_freq * Math.pow(chord["inharmonicity"],Math.log2(k+1))

        }

        gain.gain.value = sweights[j] * timbre[k]
        
        if (chord["laterals"].length>0){
          panner = tone_nodes[j][k][2]
          panner.pan.value = chord["laterals"][i] 
        }
      }
    }
  }
  ampEnv.triggerAttackRelease((chord["attack"] + chord["decay"])*(1 + chord["reg"]))
}

additive_synth_setter = function(N,shepardize,lateralize){

  NH=get_NH_NSH(shepardize)[0]
  NSH=get_NH_NSH(shepardize)[1]

  var control_nodes = {}
  var ampEnv = new Tone.AmplitudeEnvelope({
    "attack": default_dict["attack"],
    "decay": default_dict["decay"],
    "sustain": default_dict["sustain_amp"],
    "release": default_dict["duration"],
    "attackCurve" : "linear",
    "releaseCurve" : "exponential"
  }).toDestination();

  for (i=0;i<N;i++){

    var tone_nodes = util_2d_array(2*NSH+1,NH)

    for (j=0;j<2*NSH+1;j++){
      for (k=0;k<NH;k++){
        var osc = new Tone.Oscillator({"type": "sine", "volume": -17});
        var gain = new Tone.Gain();
        if (lateralize){
          var panner = new Tone.Panner(0); // controls lateralization of tones
          osc.connect(gain).start();
          gain.connect(panner);
          panner.connect(ampEnv);
          tone_nodes[j][k] = [osc,gain,panner];
        } else {
          osc.connect(gain).start();
          gain.connect(ampEnv);
          tone_nodes[j][k] = [osc,gain];
        }
      }
    }

    control_nodes["complex_" + String(i)] = tone_nodes

  }

  control_nodes["envelope"] = ampEnv

  return control_nodes

}

util_2d_array = function(M,N) {
  var matrix = new Array(M)
  for (m=0;m<matrix.length;m++) {
    matrix[m] = new Array(N)
  }
  return matrix
}

get_NH_NSH = function(shepardize) {
  if (!shepardize){
    NH = default_dict["NH"] // default_dict["NH"] should be the maximum number of harmonics that is needed for any synthesis (without crashing)
    NSH = 0 // when shepardize is false this doesn't play any role, but the setter requires it regardless, so we set it to the smallest value (to avoid too many audio nodes)
  } else {
    NH = 4 // default_dict["NH"] should be the maximum number of harmonics that is needed for any synthesis, we take it to be 4 for shepard since we need NSH=10
    NSH = default_dict["NSH"]
  }
  return [NH,NSH]
}

process_sliders = function(sliders) {
  num_of_intervals = 0
  num_of_basses = 0
  num_of_laterals = 0
  stretch = 0
  volume = 0
  mean_pitch = 0
  num_of_partials = 0
  timbre_param = ""

  for (i in sliders){
    id = sliders[i].split("_")
    if (id[0] == "bass"){num_of_basses += 1}
    if (id[0] == "int" && id[1] == "1"){num_of_intervals += 1}
    if (id[0] == "lateral" && id[1] == "1"){num_of_laterals += 1}  

    // Globals
    if (id[0] == "stretch"){stretch += 1}
    if (id[0] == "volume"){volume += 1}
    if (id[0] == "mpitch"){mean_pitch += 1}
    if (id[0] == "harmonic"){
      num_of_partials += 1
      timbre_param = id[1]
    }
    
  }

  num_of_chords = num_of_basses + 1

  return {"num_of_intervals": num_of_intervals,
          "num_of_basses": num_of_basses,
          "num_of_chords": num_of_chords,
          "num_of_laterals": num_of_laterals,
          "stretch": stretch,
          "volume": volume,
          "mean_pitch": mean_pitch,
          "num_of_partials": num_of_partials,
          "timbre_param": timbre_param
        } 
}

play_list = function(active_nodes,chords) { 
  var n = chords.length;
  var onsets = new Array(n).fill(0);

  for (i = 0; i < n; i ++) {
    chord = chords[i];
    if (i < n - 1) {
      onsets[i + 1] = onsets[i] + 0.5; // 0.7
    }
    play_chord_with_delay(active_nodes, chord, 1000 * onsets[i]);
  }
};

play_chord_with_delay = function(active_nodes, chord, delay) {
setTimeout(function() {
  play_chord(active_nodes, chord);
}, delay);
};

create_chord_list = function(slider_data,fixed_chord){

  var chord_list = []
            
  for (i=0;i<slider_data["num_of_chords"];i++){

      intervals = []
      laterals = []
      stretch = []
      volume = []
      mean_pitch = []
      custom_timbre = []
      
      if (i==0) {
          rel_bass = f0
      } else {
          rel_bass = f0 + JSON.parse(document.getElementById("bass_" + String(i+1) + "_1").value) 
      }
      if (!fixed_chord){
          for (j=0;j<slider_data["num_of_intervals"];j++){
              intervals = intervals.concat([JSON.parse(document.getElementById("int_" + String(i+1) + "_" + String(j+1)).value)]) 
          }
      } else {
          intervals = fixed_chord
      }

      for (k=0;k<slider_data["num_of_laterals"];k++){
          laterals = laterals.concat([JSON.parse(document.getElementById("lateral_" + String(i+1) + "_" + String(k+1)).value)]) 
      }

      for (s=0;s<slider_data["stretch"];s++){
          stretch = stretch.concat([JSON.parse(document.getElementById("stretch").value)])
      }

      for (r=0;r<slider_data["volume"];r++){
        volume = volume.concat([JSON.parse(document.getElementById("volume").value)])
      }

      for (rr=0;rr<slider_data["mean_pitch"];rr++){
        mean_pitch = mean_pitch.concat([JSON.parse(document.getElementById("mpitch").value)])
      }

      for (l=0;l<slider_data["num_of_partials"];l++){
        custom_timbre = custom_timbre.concat([JSON.parse(document.getElementById("harmonic_" + slider_data["timbre_param"] + "_" + String(l+1)).value)])
      }  
    
      chord_list = chord_list.concat([
        {
          "f0": rel_bass, 
          "intervals": intervals, 
          "synth": synth, 
          "parametrization": parametrization, 
          "shepardize": shepardize, 
          "laterals": laterals,
          "stretch": stretch,
          "volume": volume,
          "mean_pitch": mean_pitch,
          "custom_timbre": custom_timbre,
          "timbre_param": slider_data["timbre_param"]
        }
      ])
  }
  return chord_list
}

get_custom_freqs = function(type,NH){
  
  if (type=="gamelan"){

    freqs = [1,1.52,3.46,3.92]
    freqs = freqs.concat(new Array(NH - 4).fill(1))

  } else {

    freqs = []

  }
  
  return freqs

}

angle_to_point = function(angles){
  dim = angles.length + 1
  console.assert(dim>1, "no angles specified!")
  point = [Math.cos(angles[0])]
  sinprod = 1
  
  for (a=1;a<angles.length;a++){
    sinprod = sinprod * Math.sin(angles[a-1])
    point = point.concat([sinprod * Math.cos(angles[a])])
  }

  point = point.concat([sinprod * Math.sin(angles[angles.length - 1])])

  return point

}

norm = function(point){
  tosum = point.map(x => Math.pow(x,2))
  value = 0
  for (i=0;i<tosum.length;i++){ value = value + tosum[i] }
  return Math.sqrt(value)
}

equilibrize_chord = function(fqs,fmean){
  fqs = fqs.map(x => util_freq2midi(x))
  fmean = util_freq2midi(fmean)

  old_mean = 0
  for (kk=0;kk<fqs.length;kk++){old_mean = old_mean + fqs[kk]}
  old_mean = old_mean / fqs.length

  equif = fqs.map(x => x + (fmean - old_mean))
  equif = equif.map(x => util_midi2freq(x))

  return equif
}

const piano = new Tone.Sampler({
	urls: {
		C3: "C3.mp3",
    Db3: "Db3.mp3",
    D3: "D3.mp3",
    Eb3: "Eb3.mp3",
    E3: "E3.mp3",
    F3: "F3.mp3",
    Gb3: "Gb3.mp3",
    G3: "G3.mp3",
    Ab3: "Ab3.mp3",
    A3: "A3.mp3",
    Bb3: "Bb3.mp3",
    B3: "B3.mp3",
		C4: "C4.mp3",
    Db4: "Db4.mp3",
    D4: "D4.mp3",
    Eb4: "Eb4.mp3",
    E4: "E4.mp3",
    F4: "F4.mp3",
    Gb4: "Gb4.mp3",
    G4: "G4.mp3",
    Ab4: "Ab4.mp3",
    A4: "A4.mp3",
    Bb4: "Bb4.mp3",
    B4: "B4.mp3",
    C5: "C5.mp3",
    Db5: "Db5.mp3",
    D5: "D5.mp3",
    Eb5: "Eb5.mp3",
    E5: "E5.mp3",
    F5: "F5.mp3",
    Gb5: "Gb5.mp3",
    G5: "G5.mp3",
    Ab5: "Ab5.mp3",
    A5: "A5.mp3",
    Bb5: "Bb5.mp3",
    B5: "B5.mp3",
		C6: "C6.mp3",
	},
	baseUrl: "https://instrument-sampler.s3.amazonaws.com/soundfonts/acoustic_grand_piano-mp3/"
}).toDestination();

const violin = new Tone.Sampler({
	urls: {
		C3: "C3.mp3",
    Db3: "Db3.mp3",
    D3: "D3.mp3",
    Eb3: "Eb3.mp3",
    E3: "E3.mp3",
    F3: "F3.mp3",
    Gb3: "Gb3.mp3",
    G3: "G3.mp3",
    Ab3: "Ab3.mp3",
    A3: "A3.mp3",
    Bb3: "Bb3.mp3",
    B3: "B3.mp3",
		C4: "C4.mp3",
    Db4: "Db4.mp3",
    D4: "D4.mp3",
    Eb4: "Eb4.mp3",
    E4: "E4.mp3",
    F4: "F4.mp3",
    Gb4: "Gb4.mp3",
    G4: "G4.mp3",
    Ab4: "Ab4.mp3",
    A4: "A4.mp3",
    Bb4: "Bb4.mp3",
    B4: "B4.mp3",
    C5: "C5.mp3",
    Db5: "Db5.mp3",
    D5: "D5.mp3",
    Eb5: "Eb5.mp3",
    E5: "E5.mp3",
    F5: "F5.mp3",
    Gb5: "Gb5.mp3",
    G5: "G5.mp3",
    Ab5: "Ab5.mp3",
    A5: "A5.mp3",
    Bb5: "Bb5.mp3",
    B5: "B5.mp3",
		C6: "C6.mp3",
	},
	baseUrl: "https://instrument-sampler.s3.amazonaws.com/soundfonts/violin-mp3/"
}).toDestination();

const guitar = new Tone.Sampler({
	urls: {
		C3: "C3.mp3",
    Db3: "Db3.mp3",
    D3: "D3.mp3",
    Eb3: "Eb3.mp3",
    E3: "E3.mp3",
    F3: "F3.mp3",
    Gb3: "Gb3.mp3",
    G3: "G3.mp3",
    Ab3: "Ab3.mp3",
    A3: "A3.mp3",
    Bb3: "Bb3.mp3",
    B3: "B3.mp3",
		C4: "C4.mp3",
    Db4: "Db4.mp3",
    D4: "D4.mp3",
    Eb4: "Eb4.mp3",
    E4: "E4.mp3",
    F4: "F4.mp3",
    Gb4: "Gb4.mp3",
    G4: "G4.mp3",
    Ab4: "Ab4.mp3",
    A4: "A4.mp3",
    Bb4: "Bb4.mp3",
    B4: "B4.mp3",
    C5: "C5.mp3",
    Db5: "Db5.mp3",
    D5: "D5.mp3",
    Eb5: "Eb5.mp3",
    E5: "E5.mp3",
    F5: "F5.mp3",
    Gb5: "Gb5.mp3",
    G5: "G5.mp3",
    Ab5: "Ab5.mp3",
    A5: "A5.mp3",
    Bb5: "Bb5.mp3",
    B5: "B5.mp3",
		C6: "C6.mp3",
	},
	baseUrl: "https://instrument-sampler.s3.amazonaws.com/soundfonts/acoustic_guitar_nylon-mp3/"
}).toDestination();

const clarinet = new Tone.Sampler({
	urls: {
		C3: "C3.mp3",
    Db3: "Db3.mp3",
    D3: "D3.mp3",
    Eb3: "Eb3.mp3",
    E3: "E3.mp3",
    F3: "F3.mp3",
    Gb3: "Gb3.mp3",
    G3: "G3.mp3",
    Ab3: "Ab3.mp3",
    A3: "A3.mp3",
    Bb3: "Bb3.mp3",
    B3: "B3.mp3",
		C4: "C4.mp3",
    Db4: "Db4.mp3",
    D4: "D4.mp3",
    Eb4: "Eb4.mp3",
    E4: "E4.mp3",
    F4: "F4.mp3",
    Gb4: "Gb4.mp3",
    G4: "G4.mp3",
    Ab4: "Ab4.mp3",
    A4: "A4.mp3",
    Bb4: "Bb4.mp3",
    B4: "B4.mp3",
    C5: "C5.mp3",
    Db5: "Db5.mp3",
    D5: "D5.mp3",
    Eb5: "Eb5.mp3",
    E5: "E5.mp3",
    F5: "F5.mp3",
    Gb5: "Gb5.mp3",
    G5: "G5.mp3",
    Ab5: "Ab5.mp3",
    A5: "A5.mp3",
    Bb5: "Bb5.mp3",
    B5: "B5.mp3",
		C6: "C6.mp3",
	},
	baseUrl: "https://instrument-sampler.s3.amazonaws.com/soundfonts/clarinet-mp3/"
}).toDestination();

const harpsichord = new Tone.Sampler({
	urls: {
		C3: "C3.mp3",
    Db3: "Db3.mp3",
    D3: "D3.mp3",
    Eb3: "Eb3.mp3",
    E3: "E3.mp3",
    F3: "F3.mp3",
    Gb3: "Gb3.mp3",
    G3: "G3.mp3",
    Ab3: "Ab3.mp3",
    A3: "A3.mp3",
    Bb3: "Bb3.mp3",
    B3: "B3.mp3",
		C4: "C4.mp3",
    Db4: "Db4.mp3",
    D4: "D4.mp3",
    Eb4: "Eb4.mp3",
    E4: "E4.mp3",
    F4: "F4.mp3",
    Gb4: "Gb4.mp3",
    G4: "G4.mp3",
    Ab4: "Ab4.mp3",
    A4: "A4.mp3",
    Bb4: "Bb4.mp3",
    B4: "B4.mp3",
    C5: "C5.mp3",
    Db5: "Db5.mp3",
    D5: "D5.mp3",
    Eb5: "Eb5.mp3",
    E5: "E5.mp3",
    F5: "F5.mp3",
    Gb5: "Gb5.mp3",
    G5: "G5.mp3",
    Ab5: "Ab5.mp3",
    A5: "A5.mp3",
    Bb5: "Bb5.mp3",
    B5: "B5.mp3",
		C6: "C6.mp3",
	},
	baseUrl: "https://instrument-sampler.s3.amazonaws.com/soundfonts/harpsichord-mp3/"
}).toDestination();

const trumpet = new Tone.Sampler({
	urls: {
		C3: "C3.mp3",
    Db3: "Db3.mp3",
    D3: "D3.mp3",
    Eb3: "Eb3.mp3",
    E3: "E3.mp3",
    F3: "F3.mp3",
    Gb3: "Gb3.mp3",
    G3: "G3.mp3",
    Ab3: "Ab3.mp3",
    A3: "A3.mp3",
    Bb3: "Bb3.mp3",
    B3: "B3.mp3",
		C4: "C4.mp3",
    Db4: "Db4.mp3",
    D4: "D4.mp3",
    Eb4: "Eb4.mp3",
    E4: "E4.mp3",
    F4: "F4.mp3",
    Gb4: "Gb4.mp3",
    G4: "G4.mp3",
    Ab4: "Ab4.mp3",
    A4: "A4.mp3",
    Bb4: "Bb4.mp3",
    B4: "B4.mp3",
    C5: "C5.mp3",
    Db5: "Db5.mp3",
    D5: "D5.mp3",
    Eb5: "Eb5.mp3",
    E5: "E5.mp3",
    F5: "F5.mp3",
    Gb5: "Gb5.mp3",
    G5: "G5.mp3",
    Ab5: "Ab5.mp3",
    A5: "A5.mp3",
    Bb5: "Bb5.mp3",
    B5: "B5.mp3",
		C6: "C6.mp3",
	},
	baseUrl: "https://instrument-sampler.s3.amazonaws.com/soundfonts/trumpet-mp3/"
}).toDestination();

const flute = new Tone.Sampler({
	urls: {
		C3: "C3.mp3",
    Db3: "Db3.mp3",
    D3: "D3.mp3",
    Eb3: "Eb3.mp3",
    E3: "E3.mp3",
    F3: "F3.mp3",
    Gb3: "Gb3.mp3",
    G3: "G3.mp3",
    Ab3: "Ab3.mp3",
    A3: "A3.mp3",
    Bb3: "Bb3.mp3",
    B3: "B3.mp3",
		C4: "C4.mp3",
    Db4: "Db4.mp3",
    D4: "D4.mp3",
    Eb4: "Eb4.mp3",
    E4: "E4.mp3",
    F4: "F4.mp3",
    Gb4: "Gb4.mp3",
    G4: "G4.mp3",
    Ab4: "Ab4.mp3",
    A4: "A4.mp3",
    Bb4: "Bb4.mp3",
    B4: "B4.mp3",
    C5: "C5.mp3",
    Db5: "Db5.mp3",
    D5: "D5.mp3",
    Eb5: "Eb5.mp3",
    E5: "E5.mp3",
    F5: "F5.mp3",
    Gb5: "Gb5.mp3",
    G5: "G5.mp3",
    Ab5: "Ab5.mp3",
    A5: "A5.mp3",
    Bb5: "Bb5.mp3",
    B5: "B5.mp3",
		C6: "C6.mp3",
	},
	baseUrl: "https://instrument-sampler.s3.amazonaws.com/soundfonts/flute-mp3/"
}).toDestination();

INSTRUMENTS = {
  "piano": piano,
  "violin": violin,
  "guitar": guitar,
  "harpsichord": harpsichord,
  "clarinet": clarinet,
  "flute": flute,
  "trumpet": trumpet
}

INST_NAMES = Object.keys(INSTRUMENTS)