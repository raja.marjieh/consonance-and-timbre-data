import numpy as np

def generate_mode(mode_specs, target):
    mode = dict()
    mode["target"] = target
    
    for i in range(mode_specs["num_progressions"]+1):
        if i>0:
            mode["bass_"+str(i+1)+"_1"] = mode_specs["bass_rel_range"]
        if (not "fixed_chord" in target):
            for j in range(mode_specs["num_intervals"]+1):
                if j>0:
                    mode["int_"+str(i+1)+"_"+str(j)] = mode_specs["int_range"]
                if mode_specs["lateral"]:
                    mode["lateral_"+str(i+1)+"_"+str(j+1)] = mode_specs["lateral_range"]
        else:
            for j in range(len(target["fixed_chord"])+1):
                if mode_specs["lateral"]:
                    mode["lateral_"+str(i+1)+"_"+str(j+1)] = mode_specs["lateral_range"]

        # global properties (shared between all tones and chords)
        if mode_specs["stretch"]:
            mode["stretch"] = mode_specs["inharmonicity_range"]
        
        if mode_specs["free_timbre"]:
            for i in range(1,mode_specs["tdim"]+1):
                if mode_specs["timbre_param"] == "euclidean": 
                    mode["harmonic_euclidean_" + str(i)] = [0, 1] 
                elif mode_specs["timbre_param"] == "spherical": 
                    if i==1:
                        mode["harmonic_spherical_" + str(i)] = [0, np.pi/4] # to ensure first harmonic is always biggest (to free that constraint set to pi/2)
                    else:
                        mode["harmonic_spherical_" + str(i)] = [0, np.pi/2] # to ensure positivity of weights
                else:
                    raise Exception("Undefined parametrization!")
        
        if mode_specs["free_volume"]:
            mode["volume"] = mode_specs["volume_range"]

        if mode_specs["free_mean_pitch"]:
            mode["mpitch"] = mode_specs["mean_pitch_range"]


    keys = [key for key in mode]
    assert len(keys)>1, "You should have at least one active dimension!"  
    return mode