# consonance-repository

This repository contains all the experimental data used in the consonance study in raw and in csv formats as well as the code for exporting the data and constructing the demographic tables.

## List of Experiments

1. Study 1A. Dyadic consonance for harmonic complex tones, raw: `dyh3dd-data.zip`, csv:  `rating_dyh3dd.csv`.
2. Study 1B. Dyadic consonance for synthesized Western instruments.
    1. Flute, raw: `harflt-data.zip`, csv: `rating_flute_harmonic_harflt.csv`.
    2. Guitar, raw: `hargtr-data.zip`, csv: `rating_guitar_harmonic_hargtr.csv`.
    3. Piano, raw: `harpno-data.zip`, csv: `rating_piano_harmonic_harpno.csv `.
3. Study 2A. Dyadic pleasantness judgments for stretched and compressed tones. 
    1. Stretched, raw: `dys3dd-data.zip`, csv: `rating_dys3dd.csv`.
    2. Compressed, raw: `dyc3dd-data.zip`, csv:  `rating_dyc3dd.csv`.
4. Study 2B. Dyadic consonance as a function of stretching for Korean participants.
   1. Stretched, raw: `chord-str-k0-data.zip`, csv: `korean_dyad_str.csv`.
   2. Harmonic, raw: `chord-norm-k0-data.zip`, csv: `korean_dyad_harm.csv`.
   3. Compressed, raw: `fd-chord-cp-kr0-data.zip`, csv: `korean_dyad_comp.csv`.
5. Study 2C. Dyadic pleasantness judgments for the bonang, raw: `gamdyrt-data.zip`, csv: `gamelan_dyad_gamdyrt.csv`.
6. Study 3. Dyadic consonance as a function of roll-off, raw: `rodyrt-data.zip`, csv: `rolloff_dyad_rodyrt.csv`.
7. Study 4A. Harmonic deletion and consonance.
    1. Pure tones, raw: `purdyrt-data.zip`, csv: `pure_dyad_purdyrt.csv`.
    2. No 3rd harmonic, raw: `wo3rdd-data.zip`, csv: `rating_wo3rdd.csv`.
    3. Five equal harmonics, raw: `w3rdd-data.zip`, csv: `rating_w3rdd.csv`.
8. Study 4B. Dyadic preferences for (mis)tunings of the major 3rd, major 6th, and the octave.
    1. Major 3rd, complex tones, raw: `tun3p9-data.zip`, csv: `tuning_4_tun3p9.csv`.
    2. Major 3rd, pure tones, raw: `tunp39-data.zip`, csv: `tuning_4_tunp39.csv`.
    3. Major 6th, complex tones, raw: `tun8p9-data.zip`, csv: `tuning_9_tun8p9.csv`.
    4. Major 6th, pure tones, raw:  `tunp89-data.zip`, csv: `tuning_9_tunp89.csv`.
    5. Octave, complex tones, raw: `tunoch-data.zip`, csv: `tuning_12_tunoch.csv`.
    6. Octave, complex tones, raw: `tunocp-data.zip`, csv: `tuning_12_tunocp.csv`.
9. Study 5A. GSP for harmonic tone triads, raw: `trdh3d-data.zip`, csv: `harmonic_GSP_3db_trdh3d.csv`.
10. Study 5B. GSP for stretched and compressed tone triads.
    1. Stretched, raw: `trds3d-data.zip`, csv: `stretched_GSP_3db_trds3d.csv`.
    2. Compressed, raw: `trdc3d-data.zip`, csv: `compressed_GSP_3db_trdc3d.csv`.